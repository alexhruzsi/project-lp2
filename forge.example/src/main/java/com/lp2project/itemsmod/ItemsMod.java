package com.lp2project.itemsmod;

import net.minecraftforge.common.MinecraftForge;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lp2project.itemsmod.util.RegistryHandler;

import java.util.stream.Collectors;

@Mod("itemsmod")
public class ItemsMod
{
    
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "itemsmod";
    
    public ItemsMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

        RegistryHandler.init();
        
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
 
    }
   
}
