public class ToolClasses {
    public static String getTool(String nameInPortuguese){
        if(nameInPortuguese == "Picareta")
            return "pickaxe";
        if(nameInPortuguese == "Machado")
            return "axe";
        if(nameInPortuguese == "Pá")
            return "shovel";
        if(nameInPortuguese == "Enxada")
            return "hoe";
        if(nameInPortuguese == "Espada")
            return "sword";
        else
            return null;
    }
}
