
import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexh
 */
public class ModelBlockSchema {
    String parent = "block/cube_all";
    Map<String,String> textures = new HashMap<>();
    
    public ModelBlockSchema(Block block){
        textures.put("all", "modcraft:blocks/" + block.name);
    }
    
}
