/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ximbu
 */
public class Block extends BaseItem{
    String toolClass, stepSound;
    float resistance, hardness, speedFactor;
    int harvestLevel;
    
    public Block(String name, String imagePath, String toolClass, float resistance, float hardness, int harvestLevel, String stepSound, float speedFactor){
        super(name, imagePath);
        this.toolClass = toolClass;
        this.resistance = resistance;
        this.hardness = hardness;
        this.harvestLevel = harvestLevel;
        this.stepSound = stepSound;
        this.speedFactor = speedFactor;
    }
}
