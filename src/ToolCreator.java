
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ToolCreator extends javax.swing.JPanel {

   public DefaultListModel itemsModel;
   public List<Tool> itemsList;
    
    public ToolCreator() throws ClassNotFoundException {
        initComponents();
        this.itemsModel = new DefaultListModel();
        this.itemsModel.clear();
        initComponents();
        this.initializeImageFilter();
        this.savedItemsList.setModel(this.itemsModel);
        
        //Buscando dados no banco
        SelectRecords db = new SelectRecords();
        itemsList = db.selectAllTools();
        
        for(Tool tool : itemsList){
            this.itemsModel.add(this.itemsModel.getSize(), tool.name);
            this.savedItemsList.setModel(this.itemsModel);
        }
    }
    
     private void initializeImageFilter(){
        this.imageChooser.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter("PNG","png", "PNG file");
        this.imageChooser.addChoosableFileFilter(extFilter);
    }
    
    private void resetInputValues(){
        this.initializeImageFilter();
        this.efficiencySlider.setValue(500);
        this.nameInput.setText("");
        this.maxUsesSlider.setValue(500);
        this.damageSlider.setValue(500);
        this.harvestLevelSlider.setValue(500);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imageChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        savedItemsList = new javax.swing.JList<>();
        saveItemButton = new javax.swing.JButton();
        itemNameLabel1 = new javax.swing.JLabel();
        chooseImageButton = new javax.swing.JButton();
        maxUsesLabel = new javax.swing.JLabel();
        itemNameLabel = new javax.swing.JLabel();
        efficiencyLabel = new javax.swing.JLabel();
        nameInput = new javax.swing.JTextField();
        itemNameLabel2 = new javax.swing.JLabel();
        maxUsesSlider = new javax.swing.JSlider();
        efficiencySlider = new javax.swing.JSlider();
        itemNameLabel3 = new javax.swing.JLabel();
        selectedImagePathLabel = new javax.swing.JLabel();
        toolPicker = new javax.swing.JComboBox<>();
        itemNameLabel4 = new javax.swing.JLabel();
        damageLabel = new javax.swing.JLabel();
        damageSlider = new javax.swing.JSlider();
        itemNameLabel5 = new javax.swing.JLabel();
        harvestLevelLabel = new javax.swing.JLabel();
        itemNameLabel6 = new javax.swing.JLabel();
        harvestLevelSlider = new javax.swing.JSlider();

        imageChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                imageChooserPropertyChange(evt);
            }
        });

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setViewportView(savedItemsList);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 64, 160, 176));

        saveItemButton.setText("Salvar Item");
        saveItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveItemButtonActionPerformed(evt);
            }
        });
        add(saveItemButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 258, 160, -1));

        itemNameLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel1.setText("Adicionados");
        add(itemNameLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 26, -1, 27));

        chooseImageButton.setText("Selecionar Imagem");
        chooseImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseImageButtonActionPerformed(evt);
            }
        });
        add(chooseImageButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, 160, -1));

        maxUsesLabel.setText("500");
        add(maxUsesLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(146, 140, -1, -1));

        itemNameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel.setText("Nome");
        add(itemNameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 26, -1, 20));

        efficiencyLabel.setText("500");
        add(efficiencyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(78, 198, -1, -1));

        nameInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameInputActionPerformed(evt);
            }
        });
        nameInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nameInputKeyReleased(evt);
            }
        });
        add(nameInput, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 52, 164, -1));

        itemNameLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel2.setText("Quantidade de Usos");
        add(itemNameLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 135, -1, 20));

        maxUsesSlider.setMaximum(1000);
        maxUsesSlider.setMinimum(1);
        maxUsesSlider.setValue(500);
        maxUsesSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maxUsesSliderStateChanged(evt);
            }
        });
        maxUsesSlider.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                maxUsesSliderPropertyChange(evt);
            }
        });
        add(maxUsesSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 161, 158, -1));

        efficiencySlider.setMaximum(1000);
        efficiencySlider.setMinimum(1);
        efficiencySlider.setValue(500);
        efficiencySlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                efficiencySliderStateChanged(evt);
            }
        });
        add(efficiencySlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 219, 158, -1));

        itemNameLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel3.setText("Eficiência");
        add(itemNameLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 193, -1, 20));
        add(selectedImagePathLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 338, 147, 21));

        toolPicker.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Picareta", "Machado", "Pá", "Enxada", "Espada" }));
        add(toolPicker, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 104, 164, -1));

        itemNameLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel4.setText("Tipo");
        add(itemNameLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 78, -1, 20));

        damageLabel.setText("500");
        add(damageLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 256, -1, -1));

        damageSlider.setMaximum(1000);
        damageSlider.setMinimum(1);
        damageSlider.setValue(500);
        damageSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                damageSliderStateChanged(evt);
            }
        });
        add(damageSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 277, 158, -1));

        itemNameLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel5.setText("Dano");
        add(itemNameLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 251, -1, 20));

        harvestLevelLabel.setText("500");
        add(harvestLevelLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(158, 317, -1, -1));

        itemNameLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel6.setText("Capacidade de Coleta");
        add(itemNameLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 312, -1, 20));

        harvestLevelSlider.setMaximum(1000);
        harvestLevelSlider.setMinimum(1);
        harvestLevelSlider.setValue(500);
        harvestLevelSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                harvestLevelSliderStateChanged(evt);
            }
        });
        add(harvestLevelSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 338, 158, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void saveItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveItemButtonActionPerformed

        String name = this.nameInput.getText();
        String toolClass = ToolClasses.getTool(this.toolPicker.getSelectedItem().toString());
        int maxUses = this.maxUsesSlider.getValue();
        int efficiency = this.efficiencySlider.getValue();
        float damage = this.damageSlider.getValue();
        int harvestLevel = this.harvestLevelSlider.getValue();


        if(name!=null && toolClass!=null && this.imageChooser.getSelectedFile() != null){
            String imagePath = this.imageChooser.getSelectedFile().getAbsolutePath();
            
            //Salvar no banco
            InsertRecords db = new InsertRecords();
            try {
                db.insertTool(name, imagePath, maxUses, efficiency, damage, harvestLevel, toolClass);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ToolCreator.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            this.itemsModel.add(this.itemsModel.getSize(), name);
            this.savedItemsList.setModel(this.itemsModel);
            this.resetInputValues();
        }

    }//GEN-LAST:event_saveItemButtonActionPerformed

    private void chooseImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseImageButtonActionPerformed
        this.imageChooser.showOpenDialog(this);
    }//GEN-LAST:event_chooseImageButtonActionPerformed

    private void nameInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameInputActionPerformed

    }//GEN-LAST:event_nameInputActionPerformed

    private void nameInputKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameInputKeyReleased

    }//GEN-LAST:event_nameInputKeyReleased

    private void maxUsesSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_maxUsesSliderStateChanged
        this.maxUsesLabel.setText(Integer.toString(this.maxUsesSlider.getValue()));
    }//GEN-LAST:event_maxUsesSliderStateChanged

    private void maxUsesSliderPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_maxUsesSliderPropertyChange

    }//GEN-LAST:event_maxUsesSliderPropertyChange

    private void efficiencySliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_efficiencySliderStateChanged
        this.efficiencyLabel.setText(Integer.toString(this.efficiencySlider.getValue()));
    }//GEN-LAST:event_efficiencySliderStateChanged

    private void damageSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_damageSliderStateChanged
        this.damageLabel.setText(Integer.toString(this.damageSlider.getValue()));
    }//GEN-LAST:event_damageSliderStateChanged

    private void harvestLevelSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_harvestLevelSliderStateChanged
        this.harvestLevelLabel.setText(Integer.toString(this.harvestLevelSlider.getValue()));
    }//GEN-LAST:event_harvestLevelSliderStateChanged

    private void imageChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_imageChooserPropertyChange
         if (this.imageChooser.getSelectedFile() != null) {
            String imagePath = this.imageChooser.getSelectedFile().getPath();
            this.selectedImagePathLabel.setText((imagePath.length() > 30) ? imagePath.substring(imagePath.length() - 29) : imagePath);
        } 
    }//GEN-LAST:event_imageChooserPropertyChange


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseImageButton;
    private javax.swing.JLabel damageLabel;
    private javax.swing.JSlider damageSlider;
    private javax.swing.JLabel efficiencyLabel;
    private javax.swing.JSlider efficiencySlider;
    private javax.swing.JLabel harvestLevelLabel;
    private javax.swing.JSlider harvestLevelSlider;
    private javax.swing.JFileChooser imageChooser;
    private javax.swing.JLabel itemNameLabel;
    private javax.swing.JLabel itemNameLabel1;
    private javax.swing.JLabel itemNameLabel2;
    private javax.swing.JLabel itemNameLabel3;
    private javax.swing.JLabel itemNameLabel4;
    private javax.swing.JLabel itemNameLabel5;
    private javax.swing.JLabel itemNameLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel maxUsesLabel;
    private javax.swing.JSlider maxUsesSlider;
    private javax.swing.JTextField nameInput;
    private javax.swing.JButton saveItemButton;
    private javax.swing.JList<String> savedItemsList;
    private javax.swing.JLabel selectedImagePathLabel;
    private javax.swing.JComboBox<String> toolPicker;
    // End of variables declaration//GEN-END:variables
}
