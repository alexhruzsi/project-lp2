
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

public class BaseItemCreator extends javax.swing.JPanel {
    public BaseItem currentItem;
    public ArrayList<BaseItem> itemsList;
    public DefaultListModel itemsModel;
    
    public BaseItemCreator() {
       this.currentItem = new BaseItem("", "");
        this.itemsList = new ArrayList<>();
        this.itemsModel = new DefaultListModel();
        this.itemsModel.clear();
        initComponents();
        this.savedItemsList.setModel(this.itemsModel);
    }
    
    public void AddBaseItemsToForge(){
        ForgeFileEditor.addBaseItemsList(this.itemsList);
        this.itemsList = new ArrayList<>();
        this.itemsModel.clear();
        this.savedItemsList.setModel(this.itemsModel); 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imageChooser = new javax.swing.JFileChooser();
        itemNameTextField = new javax.swing.JTextField();
        itemNameLabel = new javax.swing.JLabel();
        chooseImageButton = new javax.swing.JButton();
        saveItemButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        savedItemsList = new javax.swing.JList<>();
        selectedImagePathLabel = new javax.swing.JLabel();
        itemNameLabel1 = new javax.swing.JLabel();

        imageChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                imageChooserPropertyChange(evt);
            }
        });

        itemNameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemNameTextFieldActionPerformed(evt);
            }
        });
        itemNameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                itemNameTextFieldKeyReleased(evt);
            }
        });

        itemNameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel.setText("Nome do Item");

        chooseImageButton.setText("Selecionar Imagem");
        chooseImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseImageButtonActionPerformed(evt);
            }
        });

        saveItemButton.setText("Salvar Item");
        saveItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveItemButtonActionPerformed(evt);
            }
        });

        savedItemsList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(savedItemsList);

        itemNameLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel1.setText("Adicionados");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(saveItemButton, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(itemNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectedImagePathLabel)
                    .addComponent(itemNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chooseImageButton, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(itemNameLabel1)
                        .addGap(0, 102, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(itemNameLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(itemNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(chooseImageButton)
                        .addGap(18, 18, 18)
                        .addComponent(selectedImagePathLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(saveItemButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void saveItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveItemButtonActionPerformed
        if (this.currentItem.name.length() > 0 && this.currentItem.imagePath.length() > 0) {
            this.itemsList.add(this.currentItem);
            this.itemsModel.add(this.itemsModel.getSize(), this.currentItem.name);
            this.savedItemsList.setModel(this.itemsModel);
            this.currentItem = new BaseItem("", "");
            this.selectedImagePathLabel.setText("");
            this.itemNameTextField.setText("");
        } 
    }//GEN-LAST:event_saveItemButtonActionPerformed

    private void chooseImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseImageButtonActionPerformed
       this.imageChooser.showOpenDialog(this);
    }//GEN-LAST:event_chooseImageButtonActionPerformed

    private void imageChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_imageChooserPropertyChange
        if (this.imageChooser.getSelectedFile() != null) {
            String imagePath = this.imageChooser.getSelectedFile().getPath();
            this.currentItem.imagePath = imagePath;
            this.selectedImagePathLabel.setText((imagePath.length() > 30) ? imagePath.substring(imagePath.length() - 29) : imagePath);
        } 
    }//GEN-LAST:event_imageChooserPropertyChange

    private void itemNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemNameTextFieldActionPerformed

    }//GEN-LAST:event_itemNameTextFieldActionPerformed

    private void itemNameTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemNameTextFieldKeyReleased
        this.currentItem.name = this.itemNameTextField.getText();
    }//GEN-LAST:event_itemNameTextFieldKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseImageButton;
    private javax.swing.JFileChooser imageChooser;
    private javax.swing.JLabel itemNameLabel;
    private javax.swing.JLabel itemNameLabel1;
    private javax.swing.JTextField itemNameTextField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton saveItemButton;
    private javax.swing.JList<String> savedItemsList;
    private javax.swing.JLabel selectedImagePathLabel;
    // End of variables declaration//GEN-END:variables
}
