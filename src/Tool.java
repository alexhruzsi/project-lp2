/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ximbu
 */
public class Tool extends BaseItem{
    
    int maxUses, efficiency, harvestLevel;
    float damage;
    String toolClass;
    
    public Tool(String name, String imagePath, int maxUses, int efficiency, float damage, int harvestLevel, String toolClass) {
        super(name, imagePath);
        this.maxUses = maxUses;
        this.efficiency = efficiency;
        this. harvestLevel = harvestLevel;
        this.damage = damage;
        this.toolClass = toolClass;
    }
}
