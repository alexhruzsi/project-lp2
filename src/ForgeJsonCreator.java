import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ForgeJsonCreator {
        private List<Block> blocksList;
        private List<Tool> toolsList;
        private SelectRecords db = new SelectRecords();
        
        public void createAllJson() throws IOException, ParseException, ClassNotFoundException{
            this.createToolsJson();
            this.createBlocksJson();
            this.generateNamesJson();
        }
        
        public void createToolsJson() throws IOException, ParseException{
            try {
                toolsList = db.selectAllTools();
                
                for(Tool item : toolsList){
                    try {
                        
                        Gson jsonFile = new Gson();
                        ModelItemSchema modelItem = new ModelItemSchema(item);
                       
                        FileWriter file = new FileWriter("mod-minecraft-forge/src/main/resources/assets/modcraft/models/item/" + item.name + ".json");
                        file.write(jsonFile.toJson(modelItem));
                        
                        file.flush();
                        file.close();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ForgeJsonCreator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ForgeJsonCreator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void createBlocksJson() throws IOException, ParseException{
            try {
                blocksList = db.selectAllBlocks();
              
                for(Block block : blocksList){
                   
                    try {
                        
                        Gson jsonFile = new Gson();
                        BlockStateSchema blockState = new BlockStateSchema(block);
                       
                        FileWriter file = new FileWriter("mod-minecraft-forge/src/main/resources/assets/modcraft/blockstates/" + block.name + ".json");
                        file.write(jsonFile.toJson(blockState));
                        file.flush();
                        file.close();
                        
                        ModelBlockSchema modelBlock = new ModelBlockSchema(block);
                        file = new FileWriter("mod-minecraft-forge/src/main/resources/assets/modcraft/models/block/" + block.name + ".json");
                        file.write(jsonFile.toJson(modelBlock));
                        file.flush();
                        file.close();                  

                        BlockItemSchema blockItem = new BlockItemSchema(block);
                        file = new FileWriter("mod-minecraft-forge/src/main/resources/assets/modcraft/models/item/" + block.name + ".json");
                        file.write(jsonFile.toJson(blockItem));
                        file.flush();
                        file.close();  
                        
                        
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ForgeJsonCreator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ForgeJsonCreator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    public void generateNamesJson() throws ClassNotFoundException{
        blocksList = db.selectAllBlocks();
        toolsList = db.selectAllTools();
        Gson jsonFile = new Gson();
        LangJsonSchema lang = new LangJsonSchema(blocksList, toolsList);
        try {
            FileWriter file = new FileWriter("mod-minecraft-forge/src/main/resources/assets/modcraft/lang/en_us.json");
            file.write(jsonFile.toJson(lang));
            file.flush();
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(ForgeJsonCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
        

}
