
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.json.simple.parser.ParseException;

public class BlockCreator extends javax.swing.JPanel {
    
    public DefaultListModel itemsModel;
    public List<Block> itemsList;
    
    public BlockCreator() throws ClassNotFoundException {
        initComponents();
        this.itemsModel = new DefaultListModel();
        this.itemsModel.clear();
        initComponents();
        this.initializeImageFilter();
        this.savedItemsList.setModel(this.itemsModel);
        
        //Buscando dados banco
        SelectRecords db = new SelectRecords();
        itemsList = db.selectAllBlocks();
        
        for(Block block : itemsList){
            this.itemsModel.add(this.itemsModel.getSize(), block.name);
            this.savedItemsList.setModel(this.itemsModel);
        }
       
    }
    
    private void initializeImageFilter(){
        this.imageChooser.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter("PNG","png", "PNG file");
        this.imageChooser.addChoosableFileFilter(extFilter);
    }
    
    private void resetInputValues(){
        this.initializeImageFilter();
        this.hardnessSlider.setValue(500);
        this.nameInput.setText("");
        this.resistanceSlider.setValue(500);
        this.harvestLevelSlider.setValue(500);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imageChooser = new javax.swing.JFileChooser();
        itemNameLabel1 = new javax.swing.JLabel();
        itemNameLabel = new javax.swing.JLabel();
        nameInput = new javax.swing.JTextField();
        itemNameLabel2 = new javax.swing.JLabel();
        itemNameLabel3 = new javax.swing.JLabel();
        itemNameLabel4 = new javax.swing.JLabel();
        resistanceSlider = new javax.swing.JSlider();
        hardnessSlider = new javax.swing.JSlider();
        stepSoundPicker = new javax.swing.JComboBox<>();
        saveItemButton = new javax.swing.JButton();
        resistenceLabel = new javax.swing.JLabel();
        hardnessLabel = new javax.swing.JLabel();
        selectedImagePathLabel = new javax.swing.JLabel();
        selectedImagePathLabel1 = new javax.swing.JLabel();
        harvestLevelLabel = new javax.swing.JLabel();
        itemNameLabel6 = new javax.swing.JLabel();
        harvestLevelSlider = new javax.swing.JSlider();
        jScrollPane1 = new javax.swing.JScrollPane();
        savedItemsList = new javax.swing.JList<>();
        speedFactorLabel = new javax.swing.JLabel();
        itemNameLabel7 = new javax.swing.JLabel();
        speedFactorSlider = new javax.swing.JSlider();
        selectedImagePathLabel2 = new javax.swing.JLabel();
        itemNameLabel8 = new javax.swing.JLabel();
        toolPicker = new javax.swing.JComboBox<>();
        chooseImageButton1 = new javax.swing.JButton();

        imageChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imageChooserActionPerformed(evt);
            }
        });
        imageChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                imageChooserPropertyChange(evt);
            }
        });

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        itemNameLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel1.setText("Adicionados");
        add(itemNameLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 26, -1, 27));

        itemNameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel.setText("Nome");
        add(itemNameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 26, -1, 20));

        nameInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameInputActionPerformed(evt);
            }
        });
        nameInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nameInputKeyReleased(evt);
            }
        });
        add(nameInput, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 52, 164, -1));

        itemNameLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel2.setText("Resistência");
        add(itemNameLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 147, -1, 20));

        itemNameLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel3.setText("Dureza");
        add(itemNameLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 205, -1, 20));

        itemNameLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel4.setText("Ferramenta ");
        add(itemNameLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 83, -1, 20));

        resistanceSlider.setMaximum(1000);
        resistanceSlider.setMinimum(1);
        resistanceSlider.setValue(500);
        resistanceSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                resistanceSliderStateChanged(evt);
            }
        });
        resistanceSlider.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                resistanceSliderPropertyChange(evt);
            }
        });
        add(resistanceSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 173, 164, -1));

        hardnessSlider.setMaximum(1000);
        hardnessSlider.setMinimum(1);
        hardnessSlider.setValue(500);
        hardnessSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                hardnessSliderStateChanged(evt);
            }
        });
        add(hardnessSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 231, 164, -1));

        stepSoundPicker.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Madeira", "Metal", "Pedra", "Terra", "Areia", "Grama" }));
        stepSoundPicker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepSoundPickerActionPerformed(evt);
            }
        });
        add(stepSoundPicker, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 430, 164, -1));

        saveItemButton.setText("Salvar Item");
        saveItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveItemButtonActionPerformed(evt);
            }
        });
        add(saveItemButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 258, 160, -1));

        resistenceLabel.setText("500");
        add(resistenceLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 152, -1, -1));

        hardnessLabel.setText("500");
        add(hardnessLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(71, 210, -1, -1));
        add(selectedImagePathLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 328, 147, 14));
        add(selectedImagePathLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(171, 366, 11, 9));

        harvestLevelLabel.setText("Madeira");
        add(harvestLevelLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, -1, 20));

        itemNameLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel6.setText("Capacidade de coleta");
        add(itemNameLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, 20));

        harvestLevelSlider.setMaximum(10);
        harvestLevelSlider.setValue(0);
        harvestLevelSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                harvestLevelSliderStateChanged(evt);
            }
        });
        add(harvestLevelSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 158, -1));

        jScrollPane1.setViewportView(savedItemsList);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 64, 160, 176));

        speedFactorLabel.setText("500");
        add(speedFactorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 340, -1, 20));

        itemNameLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel7.setText("Fator de Velocidade");
        add(itemNameLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, -1, 20));

        speedFactorSlider.setMaximum(1000);
        speedFactorSlider.setMinimum(1);
        speedFactorSlider.setValue(500);
        speedFactorSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                speedFactorSliderStateChanged(evt);
            }
        });
        add(speedFactorSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, 158, -1));
        add(selectedImagePathLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 430, 11, 9));

        itemNameLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameLabel8.setText("Som");
        add(itemNameLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, 20));

        toolPicker.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Picareta", "Machado", "Pá", "Enxada" }));
        toolPicker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolPickerActionPerformed(evt);
            }
        });
        add(toolPicker, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 105, 164, -1));

        chooseImageButton1.setText("Selecionar Imagem");
        chooseImageButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseImageButton1ActionPerformed(evt);
            }
        });
        add(chooseImageButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, 160, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void nameInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameInputActionPerformed

    }//GEN-LAST:event_nameInputActionPerformed

    private void nameInputKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameInputKeyReleased
       
    }//GEN-LAST:event_nameInputKeyReleased

    private void resistanceSliderPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_resistanceSliderPropertyChange

    }//GEN-LAST:event_resistanceSliderPropertyChange

    private void resistanceSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_resistanceSliderStateChanged
        this.resistenceLabel.setText(Integer.toString(this.resistanceSlider.getValue()));
    }//GEN-LAST:event_resistanceSliderStateChanged

    private void hardnessSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_hardnessSliderStateChanged
        this.hardnessLabel.setText(Integer.toString(this.hardnessSlider.getValue()));
    }//GEN-LAST:event_hardnessSliderStateChanged

    private void saveItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveItemButtonActionPerformed
       
       String name = this.nameInput.getText();
       String toolClass = ToolClasses.getTool(this.toolPicker.getSelectedItem().toString());
       float resistance = this.resistanceSlider.getValue();
       float hardness = this.hardnessSlider.getValue();
       int harvestLevel = this.harvestLevelSlider.getValue();
       String stepSound = this.stepSoundPicker.getSelectedItem().toString();
       float speedFactor = this.speedFactorSlider.getValue();
       
       if(name!=null && toolClass!=null && this.imageChooser.getSelectedFile()!=null){
            String imagePath = this.imageChooser.getSelectedFile().getAbsolutePath();
            
            //Salvar no banco
            InsertRecords db = new InsertRecords();
            try {
                db.insertBlock(name, imagePath, toolClass, resistance, hardness, harvestLevel, stepSound, speedFactor);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(BlockCreator.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //Adicionar na lista
            this.itemsModel.add(this.itemsModel.getSize(), name);
            this.savedItemsList.setModel(this.itemsModel);
            this.resetInputValues();
            
       }
           

    }//GEN-LAST:event_saveItemButtonActionPerformed

    private void imageChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_imageChooserPropertyChange
        

    }//GEN-LAST:event_imageChooserPropertyChange

    private void imageChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imageChooserActionPerformed
        if (this.imageChooser.getSelectedFile() != null) {
            String imagePath = this.imageChooser.getSelectedFile().getPath();
            this.selectedImagePathLabel.setText((imagePath.length() > 30) ? imagePath.substring(imagePath.length() - 29) : imagePath);
        } 
    }//GEN-LAST:event_imageChooserActionPerformed

    private void stepSoundPickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepSoundPickerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_stepSoundPickerActionPerformed

    private void speedFactorSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_speedFactorSliderStateChanged
       this.speedFactorLabel.setText(Integer.toString(this.speedFactorSlider.getValue()));
    }//GEN-LAST:event_speedFactorSliderStateChanged

    private void toolPickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolPickerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_toolPickerActionPerformed

    private void harvestLevelSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_harvestLevelSliderStateChanged
        int harvestLevel = this.harvestLevelSlider.getValue();
        switch(harvestLevel){
            case 0:
                this.harvestLevelLabel.setText("Madeira");
                return;
            case 1:
                this.harvestLevelLabel.setText("Pedra");
                return;
            case 2:
                this.harvestLevelLabel.setText("Ferro");
                return;
            case 3:
                this.harvestLevelLabel.setText("Diamante");
                return;
            case 4:
                this.harvestLevelLabel.setText("Netherite");
                return;
            default:
                String string = "Netherite";
                for(int i=0; i<(harvestLevel-4); i++)
                    string += "+";
                
                this.harvestLevelLabel.setText(string);
        }
    }//GEN-LAST:event_harvestLevelSliderStateChanged

    private void chooseImageButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseImageButton1ActionPerformed
        this.imageChooser.showOpenDialog(this);
    }//GEN-LAST:event_chooseImageButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseImageButton1;
    private javax.swing.JLabel hardnessLabel;
    private javax.swing.JSlider hardnessSlider;
    private javax.swing.JLabel harvestLevelLabel;
    private javax.swing.JSlider harvestLevelSlider;
    private javax.swing.JFileChooser imageChooser;
    private javax.swing.JLabel itemNameLabel;
    private javax.swing.JLabel itemNameLabel1;
    private javax.swing.JLabel itemNameLabel2;
    private javax.swing.JLabel itemNameLabel3;
    private javax.swing.JLabel itemNameLabel4;
    private javax.swing.JLabel itemNameLabel6;
    private javax.swing.JLabel itemNameLabel7;
    private javax.swing.JLabel itemNameLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nameInput;
    private javax.swing.JSlider resistanceSlider;
    private javax.swing.JLabel resistenceLabel;
    private javax.swing.JButton saveItemButton;
    private javax.swing.JList<String> savedItemsList;
    private javax.swing.JLabel selectedImagePathLabel;
    private javax.swing.JLabel selectedImagePathLabel1;
    private javax.swing.JLabel selectedImagePathLabel2;
    private javax.swing.JLabel speedFactorLabel;
    private javax.swing.JSlider speedFactorSlider;
    private javax.swing.JComboBox<String> stepSoundPicker;
    private javax.swing.JComboBox<String> toolPicker;
    // End of variables declaration//GEN-END:variables
}
