
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ForgeFileEditor {
    public static void addBaseItemOnRegistryHandler(BaseItem item) throws IOException {
        List<String> lines = new ArrayList<>();
        String line = null;
        try {
          File f1 = new File("forge/src/main/java/com/lp2project/itemsmod/util/RegistryHandler.java");
          FileReader fr = new FileReader(f1);
          BufferedReader br = new BufferedReader(fr);
          while ((line = br.readLine()) != null)
            lines.add(line); 
          fr.close();
          br.close();
          FileWriter fw = new FileWriter(f1);
          BufferedWriter out = new BufferedWriter(fw);
          lines.set(lines.size() - 1, "public static final RegistryObject<Item>" + item.name.toUpperCase() + "= ITEMS.register(\"" + item.name + "\", ItemBase::new);");
          lines.add("}");
          for (String s : lines) {
            out.write(s);
            out.newLine();
          } 
          out.close();
        } catch (Exception ex) {
          ex.printStackTrace();
        } 
    }
    
    public static void addBaseItemOnLanguage(BaseItem item) throws IOException {
        List<String> lines = new ArrayList<>();
        String line = null;
        try {
          File f1 = new File("forge/src/main/resources/assets/itemsmod/lang/en_us.json");
          FileReader fr = new FileReader(f1);
          BufferedReader br = new BufferedReader(fr);
          while ((line = br.readLine()) != null)
            lines.add(line); 
          fr.close();
          br.close();
          FileWriter fw = new FileWriter(f1);
          BufferedWriter out = new BufferedWriter(fw);
          lines.set(lines.size() - 1, "\"item.itemsmod." + item.name.toLowerCase() +"\":\"" + item.name.toLowerCase() + "\",");
          lines.add("}");
          for (String s : lines) {
            out.write(s);
            out.newLine();
          } 
          out.close();
        } catch (Exception ex) {
          ex.printStackTrace();
        } 
    }
    
    public static void addBaseItemJson(BaseItem item) throws IOException {
        File f1 = new File("forge/src/main/resources/assets/itemsmod/models/item/" + item.name.toLowerCase() + ".json");
        FileWriter fw = new FileWriter(f1);
        BufferedWriter out = new BufferedWriter(fw);
        out.write("{ \"parent\": \"item/generated\", \"textures\":{ \"layer0\":\"itemsmod:items/" + item.name.toLowerCase() + "\" } }");
        out.close();
    }
    
    public static void addBaseItemImage(BaseItem item) {
        Path destiny = Paths.get("forge/src/main/resources/assets/itemsmod/texture/items/" + item.name.toLowerCase() + ".png", new String[0]);
        try {
          Files.copy(Paths.get(item.imagePath, new String[0]), destiny, REPLACE_EXISTING);
        } catch (IOException e) {
          e.printStackTrace();
        } 
    }
    
    public static void addBaseItemsList(List<BaseItem> items) {
    items.forEach(item -> {
          try {
            addBaseItemOnRegistryHandler(item);
            addBaseItemJson(item);
            addBaseItemImage(item);
            addBaseItemOnLanguage(item);
          } catch (IOException ex) {
            Logger.getLogger(ForgeFileEditor.class.getName()).log(Level.SEVERE, (String)null, ex);
          } 
        });
    }
}
