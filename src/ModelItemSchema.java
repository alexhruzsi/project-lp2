
import java.util.HashMap;
import java.util.Map;

public class ModelItemSchema {
    String parent;
    Map<String,String> textures = new HashMap<>();
    
    public ModelItemSchema(Tool item){
        parent = "item/handheld";
        textures.put("layer0", "modcraft:items/" + item.name);
    }
}
