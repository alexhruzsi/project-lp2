/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexh
 */
public class SoundClasses {
    public static String getSound(String nameInPortuguese){
        if(nameInPortuguese == "Pedra")
            return "stone";
        if(nameInPortuguese == "Metal")
            return "metal";
        if(nameInPortuguese == "Madeira")
            return "wood";
        if(nameInPortuguese == "Terra")
            return "dirt";
        if(nameInPortuguese == "Areia")
            return "sand";
        if(nameInPortuguese == "Grama")
            return "grass";
        else
            return null;
    }
}
