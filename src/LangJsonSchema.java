
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author carlos
 */
public class LangJsonSchema {
    List<Map<String,String>> idNames;
    Map<String,String> tabName = new HashMap();
    
   public LangJsonSchema(List<Block> blocks, List<Tool> items){
        this.tabName.put("itemGroup.modcraft", "ModCraft");
        this.idNames.add(tabName);
        for(Block block : blocks){
            Map<String,String> idName = new HashMap();
            idName.put("block.modcraft." + block.name, block.name);
            this.idNames.add(idName);
        }
        for(Tool item : items){
            Map<String,String> idName = new HashMap();
            idName.put("item.modcraft." + item.name, item.name);
            this.idNames.add(idName);
        }
    }
    
}
