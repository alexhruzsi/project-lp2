
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public abstract class Launcher {
    
    public static boolean build(){
       boolean success = false;
        String userDirectory = System.getProperty("user.dir");
        userDirectory += "\\mod-minecraft-forge";
        
        ProcessBuilder builder = new ProcessBuilder();
        builder.directory(new File(userDirectory));
        builder.command("cmd.exe", "/c", "gradlew genIntellijRuns");
        
        builder.redirectErrorStream(true);
        
        try {
            Process process;
            process = builder.start();
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
        
            while (true) {
                line = reader.readLine();
                if (line == null) { break; }
                if(line.contains("BUILD SUCCESS"))
                    success = true;
                System.out.println(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }
    
    public static boolean run(){
       boolean success = false;
        String userDirectory = System.getProperty("user.dir");
        userDirectory += "\\mod-minecraft-forge";
        
        ProcessBuilder builder = new ProcessBuilder();
        builder.directory(new File(userDirectory));
        builder.command("cmd.exe", "/c", "gradlew runClient");
        
        builder.redirectErrorStream(true);
        
        try {
            Process process;
            process = builder.start();
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
        
            while (true) {
                line = reader.readLine();
                if (line == null) { break; }
                if(line.contains("BUILD SUCCESS"))
                    success = true;
                System.out.println(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }
}
