import java.sql.Connection;  
import java.sql.DriverManager;  
import java.sql.PreparedStatement;  
import java.sql.SQLException;

/**
 *
 * @author ximbu
 */
public class InsertRecords {
     private Connection connect() {  
        // SQLite connection string  
        String url = "jdbc:sqlite:sqlite/items.db";  
        Connection conn = null;  
        try {  
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);  
            System.out.println("Conectado.");
        } catch (SQLException | ClassNotFoundException e) {  
            System.out.println(e.getMessage());  
        }
        return conn;  
    }  
   
  
    public void insertBlock(String name, String imagePath, String toolClass, float resistance, float hardness, int harvestLevel, String stepSound, 
                            float speedFactor) throws ClassNotFoundException {  
        String sql = "INSERT INTO block (name, textureName, toolClass, resistance, hardness, harvestLevel, stepSound, speedFactor) VALUES(?,?,?,?,?,?,?,?)";  
   
        try{  
            Connection conn = this.connect();  
            PreparedStatement pstmt = conn.prepareStatement(sql);  
            pstmt.setString(1, name);  
            pstmt.setString(2, imagePath); 
            pstmt.setString(3, toolClass);
            pstmt.setFloat(4, resistance);
            pstmt.setFloat(5, hardness);
            pstmt.setInt(6, harvestLevel);
            pstmt.setString(7, stepSound);
            pstmt.setFloat(8, speedFactor);
            
            pstmt.executeUpdate();  
            conn.close();
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
        }  
    }
    
    public void insertTool(String name, String imagePath, int maxUses, int efficiency, float damage, int harvestLevel, String toolClass) throws ClassNotFoundException {  
        String sql = "INSERT INTO tool (name, textureName, maxUses, efficiency, damage, harvestLevel, toolClass) VALUES(?,?,?,?,?,?,?)";  
   
        try{  
            Connection conn = this.connect();  
            PreparedStatement pstmt = conn.prepareStatement(sql);  
            pstmt.setString(1, name);  
            pstmt.setString(2, imagePath); 
            pstmt.setInt(3, maxUses);
            pstmt.setInt(4, efficiency);
            pstmt.setFloat(5, damage);
            pstmt.setInt(6, harvestLevel);
            pstmt.setString(7, toolClass);
            
            pstmt.executeUpdate();  
            conn.close();
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
        }  
    }
}
