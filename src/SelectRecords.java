import java.sql.DriverManager;  
import java.sql.Connection;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.sql.Statement; 
import java.util.ArrayList;
import java.util.List;
import org.sqlite.JDBC;

/**
 *
 * @author ximbu
 */
public class SelectRecords {
    private Connection connect() {  
        // SQLite connection string  
        String url = "jdbc:sqlite:sqlite/items.db";  
        Connection conn = null;  
        try {  
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);  
        } catch (SQLException | ClassNotFoundException e) {  
            System.out.println("error:" + e.getMessage());  
        }
        return conn;  
    }  
   
  
    public List<Block> selectAllBlocks() throws ClassNotFoundException{  
        String sql = "SELECT * FROM block";  
          
        try {  
            System.out.println("teste:");
            Connection conn = this.connect(); 
            Statement stmt  = conn.createStatement();  
            ResultSet rs    = stmt.executeQuery(sql);  
              
            List<Block> blocks = new ArrayList<>();
            // loop through the result set  
            while (rs.next()) {  
                blocks.add(new Block(rs.getString("name"), rs.getString("textureName"), rs.getString("toolClass"), rs.getFloat("resistance"), 
                                     rs.getFloat("hardness"), rs.getInt("harvestLevel"), rs.getString("stepSound"), 
                                     rs.getFloat("speedFactor")));
            }
            conn.close();
            return blocks;
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
            return null;
        } 
    }
    
    public List<Tool> selectAllTools() throws ClassNotFoundException{  
        String sql = "SELECT * FROM tool";  
          
        try {  
            Connection conn = this.connect();  
            Statement stmt  = conn.createStatement();  
            ResultSet rs    = stmt.executeQuery(sql);  
              
            List<Tool> tools = new ArrayList<>();
            // loop through the result set  
            while (rs.next()) {  
                tools.add(new Tool(rs.getString("name"), rs.getString("textureName"), rs.getInt("maxUses"), rs.getInt("efficiency"), 
                                     rs.getFloat("damage"), rs.getInt("harvestLevel"), rs.getString("toolClass")));
            }
            conn.close();
            return tools;
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
            return null;
        } 
    }
}
