CREATE TABLE block(
    name VARCHAR(20),
    textureName VARCHAR(100),
    toolClass VARCHAR(7),
    resistance FLOAT,
    hardness FLOAT,
    harvestLevel INTEGER,
    stepSound VARCHAR(20),
    speedFactor FLOAT
);

CREATE TABLE food(
    name VARCHAR(20),
    textureName VARCHAR(100),
    healAmount FLOAT
);

CREATE TABLE tool(
    name VARCHAR(20),
    textureName VARCHAR(100),
    maxUses INTEGER,
    efficiency INTEGER,
    damage FLOAT,
    harvestLevel INTEGER,
    toolClass VARCHAR(7)
);