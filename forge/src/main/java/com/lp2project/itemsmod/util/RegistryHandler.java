package com.lp2project.itemsmod.util;

import com.lp2project.itemsmod.ItemsMod;
import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.RegistryObject;
import com.lp2project.itemsmod.items.ItemBase;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class RegistryHandler {
	
	public static final DeferredRegister<Item> ITEMS = new DeferredRegister<>(ForgeRegistries.ITEMS, ItemsMod.MOD_ID);
	
	public static void init() {
		ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
	
//Items
	public static final RegistryObject<Item> RUBY = ITEMS.register("ruby", ItemBase::new);	
public static final RegistryObject<Item>ITEMTESTE= ITEMS.register("itemteste", ItemBase::new);
}
